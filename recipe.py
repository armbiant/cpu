import shutil
import subprocess
import sys
from datetime import datetime
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import cygwin_path, run

VERSION = (
    "master_"
    + datetime.now().strftime("%Y%m%d%H%M%S")
    + "_"
    + subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).decode().strip()
)
RECIPE_DIR = dirname(__file__)


class Recipe(PackageShortRecipe):
    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="cpu2017",
            id="cpu2017",
            pretty_name="CPU2017 benchmarks",
            version=VERSION,
        )

    def all_steps(self, out_dir: str):
        bash = shutil.which("bash")
        recipe = cygwin_path(join(RECIPE_DIR, "recipe.sh"))
        run(bash, recipe, cygwin_path(out_dir), sys.argv[1], sys.argv[2])


PackageBuilder(Recipe()).make()

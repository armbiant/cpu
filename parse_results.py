#!/bin/python
import glob
import json
import os
import random
import sys
from datetime import datetime

import requests

save = False
if len(sys.argv) < 2:
    print("Unexpected number of arguments")
    exit()
if len(sys.argv) >= 3:
    save = True
    dir_to_save = sys.argv[2]
if len(sys.argv) >= 4:
    compiler = sys.argv[3]
    if "msvc" in compiler.lower():
        compiler = "msvc"
    elif "clang" in compiler.lower():
        compiler = "llvm"

auth_token = sys.argv[1]

if save:
    stdout_default = sys.stdout

ResultFiles = glob.glob("./*.csv", recursive=False)
average_dict = dict()
for ResultFile in ResultFiles:
    if save:
        fname_clean = ResultFile.replace("." + ResultFile.split(".")[-1], "")
        fname_clean = fname_clean.replace("./", "").replace(".\\", "").replace("\\", "")
        sys.stdout = open(dir_to_save + "/parsed_" + fname_clean, "w")

    print("############################")
    print()
    print("Reading", ResultFile)
    results = open(ResultFile).readlines()

    isValid = False
    isNewBlock = True
    compilerJustAdded = False
    whichBlock = 0

    full_results_benchmarks = []
    selected_results_benchmarks = []
    benchmarks = [full_results_benchmarks, selected_results_benchmarks]
    failed_benchmarks = []
    hardware_description = []
    software_description = []
    average_results_benchmarks = []

    def getBlock(header):
        header_ = header
        header = header.casefold()
        if "valid" in header:
            headerL = header.split(",")
            if int(headerL[1]) == 1:
                isValid = True
            return 1
        elif "full results table" in header:
            return 2
        elif "benchmark" in header and whichBlock == 2:
            return 2
        elif "benchmark" in header and whichBlock == 3:
            return 3
        elif "selected results table" in header:
            return 3
        elif "#" in header:
            return 4
        elif "hardware" in header:
            return 5
        elif "software" in header:
            return 6
        elif "os" in header and whichBlock == 6:
            software_description.append(header_.split(",")[1].replace('"', ""))
            return 6
        elif "cpu name" in header and whichBlock == 5:
            hardware_description.append(header_.split(",")[1].replace('"', ""))
            return 5
        return -1

    for line in results:
        line = line.strip()
        if len(line) == 0:
            isNewBlock = True
            continue
        if isNewBlock:
            whichBlock = getBlock(line)
            isNewBlock = False
        else:
            if whichBlock == 2 or whichBlock == 3:
                B = benchmarks[whichBlock - 2]
                fields = line.split(",")
                try:
                    B.append([fields[0], float(fields[2])])
                except:
                    pass
            elif whichBlock == 4:
                fields = line.split()
                if len(fields) > 2:
                    for field in fields:
                        f = field.split(".")
                        if len(f) == 2:
                            failed_benchmarks.append(field)
                            continue
                else:
                    if "-" in line:
                        break
            elif whichBlock == 5:
                fields = line.split(",")
                if len(fields):
                    if "memory" in fields[0].casefold():
                        hardware_description.append(fields[1])
            elif whichBlock == 6:
                fields = line.split(",")
                if len(fields):
                    if "build" in fields[1].casefold():
                        build = fields[1].replace('"', "")
                        software_description[0] = software_description[0] + " " + build
                        compilerJustAdded = False
                    elif "compiler" in fields[0].casefold():
                        software_description.append(fields[1].replace('"', ""))
                        compilerJustAdded = True
                    elif len(fields) == 2 and fields[0] == "" and compilerJustAdded:
                        comp = fields[1].replace('"', "")
                        software_description[1] = software_description[1] + " " + comp
                        compilerJustAdded = False

    print("Hardware ", hardware_description)
    print("Software ", software_description)

    last_benchmark = ""
    n = 0
    v = 0
    # Clean benchmarks
    for F in failed_benchmarks:
        for benchmarks_type in benchmarks:
            remove = []
            i = 0
            benchmarks_type_len = len(benchmarks_type)
            while i < benchmarks_type_len:
                if F in benchmarks_type[i]:
                    remove.append(i)
                    print("Removing ", benchmarks_type[i])
                    del benchmarks_type[i]
                    benchmarks_type_len -= 1
                else:
                    i += 1

    # Calculate average
    for bench in full_results_benchmarks:
        if bench[0] == last_benchmark:
            v = v + bench[1]
            n = n + 1
        else:
            if len(last_benchmark) > 0:
                average_results_benchmarks.append([last_benchmark, v / n, n])
                average_dict[last_benchmark] = v / n
            last_benchmark = bench[0]
            v = bench[1]
            n = 1
    if n > 0:
        average_results_benchmarks.append([last_benchmark, v / n, n])
        average_dict[last_benchmark] = v / n

    r = {
        "hardware": hardware_description,
        "software": software_description,
        "selected": selected_results_benchmarks,
        "average": average_results_benchmarks,
    }
    print(json.dumps(r))
    print()

    if save:
        sys.stdout.close()

if save:
    sys.stdout = stdout_default

root_url = "http://213.146.155.33"
if "Visual Studio" in software_description[1]:
    bid = (
        software_description[1].split(" ")[2]
        + "."
        + datetime.now().strftime("%Y%m%d%H%M%S")
    )
create_build_url = root_url + "/api/createbuild/spec/cpu2017/" + bid + "/"
r = requests.post(
    create_build_url,
    headers={"Auth-Token": auth_token},
    data={"patch_id": random.randint(1, 2000000)},
)
print("Create build returned:", r.text)
post_results_url = root_url + "/api/submit/spec/cpu2017/" + bid + "/" + compiler
r = requests.post(
    post_results_url,
    headers={"Auth-Token": auth_token},
    data={"metrics": json.dumps(average_dict)},
)
print("Submit metric returned:", r.text)
